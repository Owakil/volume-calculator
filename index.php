<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\VolumeCalculator\Volume;
use Pondit\Calculator\VolumeCalculator\Cone;
use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Cylinder;

$volume1=new Volume();
$volume2=new Volume();
var_dump($volume1);
var_dump($volume2);

$cone1 = new Cone();
$cone2 = new Cone();
var_dump($cone1);
var_dump($cone2);

$cube1 = new Cube();
$cube2 = new Cube();
var_dump($cube1);
var_dump($cube2);

$cylinder1 = new Cylinder();
$cylinder2 = new Cylinder();
var_dump($cylinder1);
var_dump($cylinder2);



